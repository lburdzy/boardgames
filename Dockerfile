FROM python:3.6.3-alpine
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
ADD Pipfile /code/
RUN echo "@edge http://dl-cdn.alpinelinux.org/alpine/edge/main" >> /etc/apk/repositories  
RUN echo "@edge http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories  
RUN echo "@edge http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories  
RUN apk update
RUN apk add gdal@edge libpq@edge postgresql-dev build-base jpeg-dev zlib-dev git gdal-dev@edge geos-dev@edge
ADD . /code/
RUN pip install pipenv
RUN pipenv install --dev