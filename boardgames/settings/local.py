from .common import *

DEBUG = True
ALLOWED_HOSTS = ['*']
SECRET_KEY = 'stv7in9uf+ic9taop5ge9b@w7*vl$+lp)n=@*c-*xyn#_iws!g '

INSTALLED_APPS += [
    'debug_toolbar',
]

MIDDLEWARE += [
    'debug_toolbar.middleware.DebugToolbarMiddleware'
]

INTERNAL_IPS = [
    '127.0.0.1',
    '0.0.0.0',
]
CORS_ORIGIN_ALLOW_ALL = True

ENABLE_SQL_LOGGING = True
if ENABLE_SQL_LOGGING:
    LOGGING = {
        'version': 1,
        'filters': {
            'require_debug_true': {
                '()': 'django.utils.log.RequireDebugTrue',
            }
        },
        'handlers': {
            'console': {
                'level': 'DEBUG',
                'filters': ['require_debug_true'],
                'class': 'logging.StreamHandler',
            }
        },
        'loggers': {
            'django.db.backends': {
                'level': 'DEBUG',
                'handlers': ['console'],
            }
        }
    }

