from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import refresh_jwt_token, obtain_jwt_token

from events.views import EventViewSet, EventTemplateViewSet, EntryRequestViewSet, \
    EntryViewSet
from notifications.views import NotificationViewSet

router = DefaultRouter()
router.register(r'events', EventViewSet)
router.register(r'event-templates', EventTemplateViewSet)
router.register(r'notifications', NotificationViewSet)
router.register(r'entry-requests', EntryRequestViewSet)
router.register(r'entries', EntryViewSet)

API_TITLE = 'boardgames'
API_DESCRIPTION = 'Dokumentacja API'

urlpatterns = [
    url(r'^api/clients/', include('clients.urls', namespace='clients')),
    url(r'^nottheadmin/', admin.site.urls),
    url(r'^api/', include(router.urls)),
    url(r'api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^auth/', include('djoser.urls')),
    url(r'^auth/api-token-refresh/$', refresh_jwt_token),
    url(r'^auth/api-token/$', obtain_jwt_token),
    url(r'^docs/', include('rest_framework_docs.urls')),

]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns.append(
        url(r'^__debug__/', include(debug_toolbar.urls))
    )
    urlpatterns.extend(
        static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    )
