from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm

from clients.models import Avatar

User = get_user_model()


class CustomUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User


class AvatarInline(admin.StackedInline):
    model = Avatar
    can_delete = True
    fk_name = 'user'
    readonly_fields = ('width', 'height')


class CustomUserAdmin(UserAdmin):
    form = CustomUserChangeForm
    inlines = [AvatarInline]
    list_filter = ['gender', 'is_active', 'is_staff', 'is_superuser']

    fieldsets = UserAdmin.fieldsets + (
        (None, {'fields': ('date_of_birth', 'gender')}),
    )

    search_fields = ('username', 'first_name', 'last_name')

    def get_list_display(self, request):
        list_display = super().get_list_display(request)
        list_display = list_display + ('gender', )
        return list_display


admin.site.register(User, CustomUserAdmin)
admin.site.register(Avatar)
