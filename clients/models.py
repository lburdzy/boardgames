import os
import uuid
from datetime import datetime

import arrow
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel
from djchoices import DjangoChoices, ChoiceItem
from imagekit.models.fields import ImageSpecField
from pilkit.processors.resize import ResizeToFit

from utils.validators import PHONE_REGEX


def get_avatar_file_path(_instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('uploads/avatars', filename)


class Genders(DjangoChoices):
    FEMALE = ChoiceItem(0, _('Female'))
    MALE = ChoiceItem(1, _('Male'))


class User(AbstractUser):
    gender = models.PositiveSmallIntegerField(
        verbose_name=_('gender'), choices=Genders.choices,
        null=True, blank=True
    )
    date_of_birth = models.DateField(
        verbose_name=_('date of birth'), default=datetime.now
    )
    rating = models.FloatField(default=0, verbose_name=_('rating'))
    phone_number = models.CharField(max_length=30, validators=[PHONE_REGEX], blank=True)

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __str__(self):
        return self.username

    @property
    def age(self):
        today = arrow.now().date()
        return today.year - self.date_of_birth.year - (
            (today.month, today.day)
            < (self.date_of_birth.month, self.date_of_birth.day)
        )

    @property
    def avatar_absolute_url(self):
        try:
            self.avatar
        except Avatar.DoesNotExist:
            return None
        else:
            return self.avatar.image.url


class Avatar(TimeStampedModel):
    image = models.ImageField(
        upload_to=get_avatar_file_path, height_field='height', width_field='width',
        verbose_name=_('image'),
    )
    thumbnail = ImageSpecField(
        source='image', processors=[ResizeToFit(200, 200)], format='png'
    )
    height = models.PositiveSmallIntegerField(verbose_name=_('height'))
    width = models.PositiveSmallIntegerField(verbose_name=_('width'))
    user = models.OneToOneField(
        'User', on_delete=models.SET_NULL, related_name='avatar',
        verbose_name=_('user'), null=True, blank=True, unique=True
    )
