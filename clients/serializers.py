from djoser.serializers import UserCreateSerializer
from rest_framework import serializers

from clients.models import User, Avatar


class CustomUserRegistrationSerializer(UserCreateSerializer):

    class Meta(UserCreateSerializer.Meta):
        fields = UserCreateSerializer.Meta.fields + (
            'gender', 'date_of_birth',
        )


class AvatarSerializer(serializers.HyperlinkedModelSerializer):
    thumbnail = serializers.ImageField(read_only=True)

    class Meta:
        model = Avatar
        fields = ('image', 'height', 'width', 'thumbnail')
        read_only_fields = ('height', 'width', 'thumbnail')


class UserSerializer(serializers.HyperlinkedModelSerializer):
    avatar = AvatarSerializer()

    class Meta:
        model = User
        fields = (
            'username', 'gender', 'age', 'rating', 'avatar'
        )


class UserProfileSerializer(serializers.HyperlinkedModelSerializer):
    avatar = AvatarSerializer()

    class Meta:
        model = User
        fields = (
            'username', 'first_name', 'last_name', 'date_of_birth',
            'phone_number', 'gender', 'avatar',
        )
        read_only_fields = ('username', )

    def update(self, instance, validated_data):
        avatar_data = validated_data.pop('avatar')
        instance.avatar.delete()
        instance.avatar = Avatar.objects.create(user=instance, **avatar_data)
        return super().update(instance, validated_data)
