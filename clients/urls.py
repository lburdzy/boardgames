from django.conf.urls import url

from clients.views import UserProfileView

urlpatterns = [
    url(r'^profile/$', UserProfileView.as_view(), name='profile'),
]
