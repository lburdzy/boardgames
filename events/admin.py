from django.contrib import admin
from django.contrib.admin.options import StackedInline

from events.models import Event, EventTemplate
from events.models import Entry, EntryRequest


class EntryInline(StackedInline):
    model = Entry
    extra = 0


class EntryRequestInline(StackedInline):
    model = EntryRequest
    extra = 0


class EventAdmin(admin.ModelAdmin):
    inlines = [EntryRequestInline, EntryInline]
    list_display = (
        'title', 'datetime', 'city', 'organizer', 'participants_count', 'capacity',
        'is_full',
    )
    list_filter = (
        'title', 'datetime', 'city', 'capacity',
    )
    search_fields = (
        'title', 'city', 'organizer',
    )
    autocomplete_fields = [
        'organizer'
    ]

    def is_full(self, obj):
        # pylint: disable=no-self-use
        return obj.participants_count >= obj.capacity
    is_full.boolean = True


class EventTemplateAdmin(admin.ModelAdmin):
    list_display = (
        'title', 'city', 'author', 'capacity',
    )
    list_filter = (
        'city', 'capacity',
    )
    search_fields = (
        'title', 'city', 'author',
    )


admin.site.register(Event, EventAdmin)
admin.site.register(EventTemplate, EventTemplateAdmin)
admin.site.register(Entry)
admin.site.register(EntryRequest)
