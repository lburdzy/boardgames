from business_logic.errors import LogicErrors
from business_logic.exceptions import NotPermittedException


class EventErrors(LogicErrors):
    USER_IS_NOT_EVENT_OWNER = NotPermittedException(
        'User is not an event organizer'
    )
