import arrow
from business_logic.core import validated_by
from django.conf import settings
from django.contrib.gis.db import models
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos.point import Point
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel
from location_field.models.spatial import LocationField

from events import validators
from utils.helpers import get_point_geometry
from utils.loggers import zs_logger
from utils.validators import PHONE_REGEX


class AbstractBaseEvent(models.Model):
    title = models.CharField(max_length=200, verbose_name=_('title'))
    capacity = models.PositiveSmallIntegerField(
        blank=True, null=True, verbose_name=_('participants capacity')
    )
    description = models.TextField(blank=True, verbose_name=_('description'))

    phone_number = models.CharField(max_length=30, validators=[PHONE_REGEX], blank=True)
    city = models.CharField(max_length=200, blank=True, verbose_name=_('city'))
    street = models.CharField(max_length=200, blank=True, verbose_name=_('street'))
    house_number = models.CharField(
        max_length=200, blank=True, verbose_name=_('house number')
    )
    flat_number = models.CharField(
        max_length=200, blank=True, verbose_name=_('flat number')
    )
    location = LocationField(
        verbose_name=_('location'), srid=4326, based_fields=['city'],
        zoom=7, default=Point(52.0, 26.0)
    )

    class Meta:
        abstract = True


class EventManager(models.Manager):
    def active(self):
        return self.filter(datetime__gte=timezone.now())

    def get_nearest_events(self, latitude, longitude):
        point = get_point_geometry(latitude, longitude)
        return self.annotate(distance=Distance('location', point)).order_by('distance')


class Event(AbstractBaseEvent, TimeStampedModel):
    datetime = models.DateTimeField(
        verbose_name=_('date and time')
    )

    organizer = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_('organizer'),
        related_name='events_organized'
    )
    participants = models.ManyToManyField(
        settings.AUTH_USER_MODEL, blank=True, verbose_name=_('participants'),
        related_name='events_participated_in', through='events.Entry'
    )

    # should be a temporary spot for entries
    applicants = models.ManyToManyField(
        settings.AUTH_USER_MODEL, blank=True, verbose_name=_('applicants'),
        related_name='events_applying_to', through='events.EntryRequest'
    )

    objects = EventManager()

    class Meta:
        verbose_name = _('event')
        verbose_name_plural = _('events')
        ordering = ('datetime',)

    def __str__(self):
        return ('{title}, taking place at '
                '{datetime:%h:%m} on '
                '{datetime:%a, %d-%b-%y}').format(
            title=self.title,
            datetime=self.datetime
        )

    @property
    def participants_count(self):
        return self.participants.count()

    @property
    def owner(self):
        return self.organizer

    def humanized_datetime(self, locale='pl'):
        arrow_obj = arrow.get(self.datetime)
        return arrow_obj.humanize(locale=locale)


class EventTemplate(AbstractBaseEvent, TimeStampedModel):
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_('author'),
        related_name='event_templates'
    )

    class Meta:
        verbose_name = _('event template')
        verbose_name_plural = _('event templates')

    def __str__(self):
        return '{} template by {}'.format(self.title, self.author)

    @property
    def owner(self):
        return self.author


class EntryBase(TimeStampedModel):
    user = models.ForeignKey('clients.User', verbose_name=_('user'))
    event = models.ForeignKey(Event, verbose_name=_('event'))

    class Meta:
        abstract = True


class Entry(EntryBase):
    class Meta:
        verbose_name = _('entry')
        verbose_name_plural = _('entries')
        unique_together = ('user', 'event')

    def __str__(self):
        return 'Entry for {} to {}'.format(self.user, self.event)


class EntryRequest(EntryBase):
    class Meta:
        verbose_name = _('entry request')
        verbose_name_plural = _('entry requests')
        unique_together = ('user', 'event')

    def __str__(self):
        return 'entry request for {} to {}'.format(self.user, self.event)

    @validated_by(validators.can_accept_and_reject_entry_request)
    def accept(self, _user):
        entry = Entry.objects.create(user=self.user, event=self.event)
        zs_logger.info(
            'Entry request {} from user {} '
            'to event {} has been accepted as Entry {}'.format(
                self.id, self.user, self.event, entry.id
            )
        )
        self.delete()

    @validated_by(validators.can_accept_and_reject_entry_request)
    def reject(self, _user):
        zs_logger.info(
            'Entry request {} from user {} to event {} has been rejected'.format(
                self.id, self.user, self.event
            )
        )
        self.delete()
