from django.utils.translation import ugettext as _
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from clients.serializers import UserSerializer
from events.models import Event, EventTemplate, EntryRequest, Entry
from utils.permissions import IsOwnerOrReadOnly, IsOwner


class EventTemplateSerializer(serializers.HyperlinkedModelSerializer):
    author = serializers.ReadOnlyField(source='author.username')
    permission_classes = [IsOwner]

    class Meta:
        model = EventTemplate
        fields = (
            'url', 'title', 'description', 'phone_number',
            'city', 'street', 'house_number', 'flat_number',
            'location', 'author', 'capacity'
        )

    def create(self, validated_data):
        try:
            author = self.context['request'].user
            return EventTemplate.objects.create(author=author, **validated_data)
        except (KeyError, AttributeError):
            raise ValidationError(_('There was a problem with request'))


class EventSerializer(serializers.HyperlinkedModelSerializer):
    organizer = UserSerializer(read_only=True)
    humanized_datetime = serializers.ReadOnlyField()
    participants = UserSerializer(many=True, read_only=True)
    distance = serializers.SerializerMethodField()

    permission_classes = [IsOwnerOrReadOnly]

    class Meta:
        model = Event
        fields = (
            'url', 'id', 'title', 'capacity', 'description', 'phone_number',
            'city', 'street', 'house_number', 'flat_number',
            'location', 'datetime', 'humanized_datetime', 'organizer',
            'participants', 'distance'
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        context = kwargs.get('context')
        if context:
            request = context.get('request')
            self.lat = request.GET.get('lat')
            self.lng = request.GET.get('lng')
            if not (self.lat and self.lng):
                self.fields.pop('distance')

    @staticmethod
    def get_distance(obj):
        return obj.distance.m

    def create(self, validated_data):
        organizer = self.context['request'].user
        event = Event.objects.create(organizer=organizer, **validated_data)
        return event


class EventPreviewSerializer(EventSerializer):
    class Meta(EventSerializer.Meta):
        fields = (
            'url', 'id', 'title', 'capacity', 'description',
            'city', 'street',
            'datetime', 'humanized_datetime', 'organizer',
            'participants', 'distance'
        )


class BaseEntrySerializer(serializers.HyperlinkedModelSerializer):
    user = serializers.SerializerMethodField()

    @staticmethod
    def get_user(obj):
        return obj.user_id


class EntryRequestSerializer(BaseEntrySerializer):
    class Meta:
        model = EntryRequest
        fields = ['url', 'event', 'user']


class EntrySerializer(BaseEntrySerializer):
    class Meta:
        model = Entry
        fields = ['url', 'event', 'user']
