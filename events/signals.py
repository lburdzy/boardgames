from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver

from events.models import EntryRequest
from notifications.models import Notification, NotificationTypes


@receiver(post_save, sender=EntryRequest)
def create_entry_request_notification(sender, instance, created, **_kwargs):  # noqa
    if created:
        Notification.objects.create(
            content_object=instance,
            type=NotificationTypes.SIGNUP_REQUEST_CREATED,
            owner=instance.event.owner
        )
