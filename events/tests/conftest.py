# pylint: disable=redefined-outer-name
import arrow
import pytest
from model_mommy import mommy
from rest_framework.test import APIRequestFactory

from clients.models import User
from events.models import Event, EventTemplate, Entry, EntryRequest
from events.views import EventViewSet, EventTemplateViewSet, EntryRequestViewSet
from utils.helpers import get_point_geometry


@pytest.fixture(scope='function')
def owner():
    return mommy.make(User, username='owner')


@pytest.fixture(scope='function')
def non_participant_user():
    return mommy.make(User)


@pytest.fixture(scope='function')
def participants(owner):
    users = mommy.make(User, _quantity=3)
    users.append(owner)
    return users


@pytest.fixture(scope='function')
def event(owner, participants):
    return mommy.make(Event, organizer=owner, participants=participants)


@pytest.fixture(scope='function')
def locations():
    return [get_point_geometry(0, lng) for lng in range(0, 30, 5)]


@pytest.fixture(scope='function')
def events(owner, participants, locations):
    return [
        mommy.make(
            Event, organizer=owner, participants=participants, location=location,
            datetime=arrow.now().shift(days=+7).datetime
        ) for location in locations
    ]


@pytest.fixture(scope='function')
def active_event(owner, participants):
    return mommy.make(
        Event, organizer=owner, participants=participants,
        datetime=arrow.now().shift(days=+7).datetime
    )


@pytest.fixture(scope='function')
def inactive_event(owner, participants):
    return mommy.make(
        Event, organizer=owner, participants=participants,
        datetime=arrow.now().shift(days=-7).datetime
    )


@pytest.fixture(scope='function')
def event_template(owner):
    return mommy.make(EventTemplate, author=owner)


@pytest.fixture(scope='function')
def entry(event, owner):
    return Entry.objects.get(event=event, user=owner)


@pytest.fixture(scope='function')
def entry_request(event, non_participant_user):
    return EntryRequest.objects.create(event=event, user=non_participant_user)


@pytest.fixture(scope='module')
def event_viewset():
    return EventViewSet.as_view(
        actions={'get': 'list', 'post': 'create'}
    )


@pytest.fixture(scope='function')
def entry_request_viewset_with_accept():
    return EntryRequestViewSet.as_view(
        actions={'post': 'accept'}
    )


@pytest.fixture(scope='function')
def entry_request_viewset_with_reject():
    return EntryRequestViewSet.as_view(
        actions={'post': 'reject'}
    )


@pytest.fixture(scope='module')
def event_template_viewset():
    return EventTemplateViewSet.as_view(
        actions={'get': 'list', 'post': 'create'}
    )


@pytest.fixture(scope='module')
def request_factory():
    return APIRequestFactory()
