import arrow


def result_is_event(result, event):
    mappings = {
        'id': 'id',
        'title': 'title',
        'description': 'description',
    }
    for result_attr, event_attr in mappings.items():
        if not result.get(result_attr) == getattr(event, event_attr):
            return False
    if arrow.get(result.get('datetime')) != event.datetime:
        return False
    return True


def event_in_results(results, event):
    for result in results:
        if result_is_event(result, event):
            return True
    return False


def result_is_object(result, obj):
    mappings = {
        'id': 'id',
    }
    for result_attr, obj_attr in mappings.items():
        if not result.get(result_attr) == getattr(obj, obj_attr):
            return False
    return True


def notification_in_results(results, entry_request):
    for result in results:
        if result_is_object(result, entry_request):
            return True
    return False


def get_locations_list(results):
    locations = []
    for event in results:
        coordinates = event['location']['coordinates']
        locations.append((coordinates[1], coordinates[0]))
    return locations
