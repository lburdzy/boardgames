import pytest
from business_logic.exceptions import NotPermittedException

from events.models import EntryRequest, Entry


@pytest.mark.django_db
def test_string_representation(entry):
    assert str(entry.user) in str(entry)


@pytest.mark.django_db
def test_accept_method_succeeds_for_owner(entry_request, owner):
    entry_request_id = entry_request.id
    assert entry_request.accept(owner) is None
    assert not entry_request.id
    with pytest.raises(EntryRequest.DoesNotExist):
        EntryRequest.objects.get(id=entry_request_id)
    assert Entry.objects.get(
        user=entry_request.user, event=entry_request.event
    )


@pytest.mark.django_db
def test_accept_method_fails_for_non_owner(entry_request, non_participant_user):
    assert EntryRequest.objects.get(id=entry_request.id)
    with pytest.raises(NotPermittedException):
        entry_request.accept(non_participant_user)
    with pytest.raises(Entry.DoesNotExist):
        Entry.objects.get(
            user=entry_request.user, event=entry_request.event
        )


@pytest.mark.django_db
def test_reject_method_succeeds_for_owner(entry_request, owner):
    entry_request_id = entry_request.id
    assert entry_request.reject(owner) is None
    assert not entry_request.id
    with pytest.raises(EntryRequest.DoesNotExist):
        EntryRequest.objects.get(id=entry_request_id)
    with pytest.raises(Entry.DoesNotExist):
        Entry.objects.get(user=entry_request.user, event=entry_request.event)


@pytest.mark.django_db
def test_reject_method_fails_for_non_owner(entry_request, non_participant_user):
    assert EntryRequest.objects.get(id=entry_request.id)
    with pytest.raises(NotPermittedException):
        entry_request.reject(non_participant_user)
    with pytest.raises(Entry.DoesNotExist):
        Entry.objects.get(
            user=entry_request.user, event=entry_request.event
        )
