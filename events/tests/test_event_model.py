import pytest
import arrow
from events.models import Event


@pytest.mark.django_db
def test_event_has_owner(event, owner):
    assert event.owner == owner


@pytest.mark.django_db
def test_owner_set_to_organizer(event):
    assert event.organizer == event.owner


@pytest.mark.django_db
def test_active_manager_returns_only_active_events():
    now = arrow.utcnow().datetime
    for event in Event.objects.active():
        assert event.datetime >= now


@pytest.mark.django_db
def test_participants_count_property():
    for event in Event.objects.all():
        assert event.participants_count == event.participants.count()
