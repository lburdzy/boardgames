import pytest


@pytest.mark.django_db
def test_string_representation(event_template):
    assert event_template.title in str(event_template)


@pytest.mark.django_db
def test_owner_set_to_author(event_template):
    assert event_template.author is event_template.owner
