import pytest
from django.urls.base import reverse
from rest_framework import status
from rest_framework.test import force_authenticate


@pytest.mark.django_db
def test_list_view_for_anonymous_user(event_template_viewset, request_factory):
    request = request_factory.get(reverse('eventtemplate-list'))
    response = event_template_viewset(request)
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.django_db
def test_list_view_for_authenticated_user(
        event_template_viewset, request_factory, owner):
    request = request_factory.get(reverse('eventtemplate-list'))
    force_authenticate(request, user=owner)
    response = event_template_viewset(request)
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_create_view(event_template_viewset, request_factory, owner):
    request_content = {
        'title': 'klj',
    }
    request = request_factory.post(reverse('eventtemplate-list'), request_content)
    force_authenticate(request, user=owner)
    response = event_template_viewset(request)
    assert response.status_code == status.HTTP_201_CREATED
