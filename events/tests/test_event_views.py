import json
from collections import OrderedDict

import pytest
from business_logic.exceptions import NotPermittedException
from django.urls.base import reverse
from rest_framework import status
from rest_framework.test import force_authenticate

from .helpers import event_in_results, get_locations_list


@pytest.mark.django_db
def test_list_view_for_anonymous_user(event_viewset, request_factory):
    request = request_factory.get(reverse('event-list'))
    response = event_viewset(request)
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.django_db
def test_list_view_for_authenticated_user(
        request_factory, event_viewset, non_participant_user
):
    request = request_factory.get(reverse('event-list'))
    force_authenticate(request, user=non_participant_user)
    response = event_viewset(request)
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_list_view_contains_active_event(
        request_factory, event_viewset, non_participant_user, active_event
):
    request = request_factory.get(reverse('event-list'))
    force_authenticate(request, user=non_participant_user)
    response = event_viewset(request)
    response.render()
    response_dict = json.loads(response.content)
    results = response_dict.get('results')
    assert event_in_results(results, active_event)


@pytest.mark.django_db
def test_list_view_does_not_contain_inactive_event(
        request_factory, event_viewset, non_participant_user, inactive_event
):
    request = request_factory.get(reverse('event-list'))
    force_authenticate(request, user=non_participant_user)
    response = event_viewset(request)
    response.render()
    response_dict = json.loads(response.content)
    results = response_dict.get('results')
    assert not event_in_results(results, inactive_event)


@pytest.mark.django_db
def test_list_view_with_location_is_sorted_by_distance(
        request_factory, event_viewset, non_participant_user, events
):
    # pylint: disable=unused-argument
    location = {'lat': 0, 'lng': 0}
    request = request_factory.get(
        '{}?lat={lat}&lng={lng}'.format(
            reverse('event-list'),
            **location
        )
    )
    force_authenticate(request, user=non_participant_user)
    response = event_viewset(request)
    response.render()
    response_dict = json.loads(response.content, object_pairs_hook=OrderedDict)
    results = response_dict.get('results')
    sorted_results = sorted(results, key=lambda k: k['distance'])
    assert results == sorted_results
    assert results


@pytest.mark.django_db
def test_create_view(event_viewset, request_factory, owner):
    request_content = {
        'title': 'klj',
        'datetime': '2027-12-31T23:00:00Z'
    }
    request = request_factory.post(reverse('event-list'), request_content)
    force_authenticate(request, user=owner)
    response = event_viewset(request)
    assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.django_db
def test_list_view_boundaries(
        request_factory, event_viewset, non_participant_user, events
):
    # pylint: disable=unused-argument
    boundaries = {'north': 10, 'west': -10, 'east': 10, 'south': -10}
    request_url = '{}?north={north}&west={west}&east={east}&south={south}'.format(
        reverse('event-list'),
        **boundaries
    )
    request = request_factory.get(request_url)
    force_authenticate(request, user=non_participant_user)
    response = event_viewset(request)
    response.render()
    response_dict = json.loads(response.content, object_pairs_hook=OrderedDict)
    results = response_dict.get('results')
    locations = get_locations_list(results)
    assert locations
    for location in locations:
        assert -10 <= location[0] <= 10
        assert -10 <= location[1] <= 10


@pytest.mark.django_db
def test_accept_entry_request_for_owner(
        request_factory, owner, entry_request,
        entry_request_viewset_with_accept
):
    request = request_factory.post(reverse(
        'entryrequest-detail', kwargs={'pk': entry_request.pk}
    ))
    force_authenticate(request, user=owner)
    response = entry_request_viewset_with_accept(request, pk=entry_request.pk)
    assert response.status_code == status.HTTP_200_OK
    assert response.data.get('message') == 'Entry request has been accepted'


@pytest.mark.django_db
def test_accept_entry_request_for_non_owner(
        request_factory, non_participant_user, entry_request,
        entry_request_viewset_with_accept
):
    request = request_factory.post(reverse(
        'entryrequest-detail', kwargs={'pk': entry_request.pk}
    ))
    force_authenticate(request, user=non_participant_user)
    with pytest.raises(NotPermittedException):
        entry_request_viewset_with_accept(request, pk=entry_request.pk)


@pytest.mark.django_db
def test_reject_entry_request_for_owner(
        request_factory, owner, entry_request,
        entry_request_viewset_with_reject
):
    request = request_factory.post(reverse(
        'entryrequest-detail', kwargs={'pk': entry_request.pk}
    ))
    force_authenticate(request, user=owner)
    response = entry_request_viewset_with_reject(request, pk=entry_request.pk)
    assert response.status_code == status.HTTP_200_OK
    assert response.data.get('message') == 'Entry request has been rejected'


@pytest.mark.django_db
def test_reject_entry_request_for_non_owner(
        request_factory, non_participant_user, entry_request,
        entry_request_viewset_with_reject
):
    request = request_factory.post(reverse(
        'entryrequest-detail', kwargs={'pk': entry_request.pk}
    ))
    force_authenticate(request, user=non_participant_user)
    with pytest.raises(NotPermittedException):
        entry_request_viewset_with_reject(request, pk=entry_request.pk)
