from business_logic.core import validator

from events.errors import EventErrors


@validator
def can_accept_and_reject_entry_request(entry_request, user):
    if not entry_request.event.owner == user:
        raise EventErrors.USER_IS_NOT_EVENT_OWNER
