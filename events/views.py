from django.utils.translation import ugettext as _
from rest_framework import viewsets
from rest_framework.exceptions import ValidationError
from rest_framework.decorators import detail_route
from rest_framework.response import Response

from events.models import Event, EventTemplate, Entry, EntryRequest
from events.serializers import EventSerializer, EventTemplateSerializer, \
    EntryRequestSerializer, EntrySerializer
from utils.permissions import IsEventOwner
from utils.helpers import get_polygon_geometry


class EventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.active().prefetch_related(
        'participants',
        'participants__avatar',
        'applicants',
        'applicants__avatar',
        'organizer',
        'organizer__avatar',
    )
    serializer_class = EventSerializer
    filter_fields = [
        'title', 'city', 'datetime', 'organizer', 'applicants', 'participants', 'street'
    ]
    search_fields = ['title', 'city', 'street']

    def get_serializer_class(self):
        if self.request.user:
            serializer_class = super().get_serializer_class()

        return serializer_class

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = self.filter_by_location(self.request.query_params, queryset)
        queryset = self.filter_by_boundaries(self.request.query_params, queryset)
        return queryset

    @staticmethod
    def filter_by_boundaries(query_params, queryset):
        north = query_params.get('north', None)
        south = query_params.get('south', None)
        west = query_params.get('west', None)
        east = query_params.get('east', None)
        boundaries = (north, west, east, south)
        if any(f is not None for f in boundaries):
            try:
                boundaries = list(map(float, boundaries))
                polygon = get_polygon_geometry(*boundaries)
                queryset = queryset.filter(location__within=polygon)
            except (TypeError, ValueError):
                raise ValidationError(_('Invalid boundaries were given'))
        return queryset

    @staticmethod
    def filter_by_location(query_params, queryset):
        latitude = query_params.get('lat', None)
        longitude = query_params.get('lng', None)
        if latitude and longitude:
            try:
                queryset = Event.objects.get_nearest_events(
                    float(latitude), float(longitude)
                )
            except ValueError:
                raise ValidationError(_('Invalid coordinates were given'))
        return queryset


class EventTemplateViewSet(viewsets.ModelViewSet):
    serializer_class = EventTemplateSerializer
    queryset = EventTemplate.objects.all().order_by('modified')

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(author=self.request.user)


class EntryRequestViewSet(viewsets.ModelViewSet):
    serializer_class = EntryRequestSerializer
    queryset = EntryRequest.objects.all()

    @detail_route(methods=['post', 'get'], permission_classes=[IsEventOwner])
    def accept(self, request, *_args, **_kwargs):
        entry = self.get_object()
        entry.accept(request.user)
        return Response({'message': 'Entry request has been accepted'})

    @detail_route(methods=['post', 'get'], permission_classes=[IsEventOwner])
    def reject(self, request, *_args, **_kwargs):
        entry = self.get_object()
        entry.reject(request.user)
        return Response({'message': 'Entry request has been rejected'})


class EntryViewSet(viewsets.ModelViewSet):
    serializer_class = EntrySerializer
    queryset = Entry.objects.all()
