# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-14 12:49
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django_extensions.db.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('events', '0007_auto_20170814_1332'),
    ]

    operations = [
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('type', models.PositiveSmallIntegerField(choices=[(0, 'User signed up'), (1, 'Signup request was accepted'), (2, 'Signup request was rejected'), (3, 'Rating request')], verbose_name='type')),
                ('sent', models.BooleanField(default=False, editable=False, verbose_name='sent')),
                ('handled', models.BooleanField(default=False, editable=False, verbose_name='handled')),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='events.Event', verbose_name='event')),
                ('subject_user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='notifications_about', to=settings.AUTH_USER_MODEL, verbose_name='subject user')),
                ('target_user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='notifications', to=settings.AUTH_USER_MODEL, verbose_name='target user')),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'get_latest_by': 'modified',
                'abstract': False,
            },
        ),
    ]
