# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-14 13:41
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('notifications', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='notification',
            options={'verbose_name': 'notification', 'verbose_name_plural': 'notifications'},
        ),
        migrations.RemoveField(
            model_name='notification',
            name='event',
        ),
        migrations.RemoveField(
            model_name='notification',
            name='subject_user',
        ),
        migrations.RemoveField(
            model_name='notification',
            name='target_user',
        ),
        migrations.AddField(
            model_name='notification',
            name='content_type',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='notification',
            name='object_id',
            field=models.PositiveIntegerField(default=1),
            preserve_default=False,
        ),
    ]
