# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-20 09:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notifications', '0002_auto_20170814_1541'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notification',
            name='type',
            field=models.PositiveSmallIntegerField(choices=[(0, 'Signup request was created'), (1, 'Signup request was accepted'), (2, 'Signup request was rejected'), (3, 'Rating request')], verbose_name='type'),
        ),
    ]
