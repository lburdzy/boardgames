from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel
from djchoices.choices import DjangoChoices, ChoiceItem


class NotificationTypes(DjangoChoices):
    SIGNUP_REQUEST_CREATED = ChoiceItem(0, _('Signup request was created'))
    SIGNUP_REQUEST_ACCEPTED = ChoiceItem(1, _('Signup request was accepted'))
    SIGNUP_REQUEST_REJECTED = ChoiceItem(2, _('Signup request was rejected'))
    RATING_REQUEST = ChoiceItem(3, _('Rating request'))


class Notification(TimeStampedModel):
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_('owner'),
        related_name='notifications'
    )
    type = models.PositiveSmallIntegerField(
        verbose_name=_('type'), choices=NotificationTypes.choices
    )
    sent = models.BooleanField(
        verbose_name=_('sent'), default=False, blank=True, editable=False
    )
    handled = models.BooleanField(
        verbose_name=_('handled'), default=False, blank=True, editable=False
    )
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    class Meta:
        verbose_name = _('notification')
        verbose_name_plural = _('notifications')

    def __str__(self):
        return 'Notification for: {} ({})'.format(self.type_label, self.content_object)

    @property
    def type_label(self):
        return NotificationTypes.get_choice(self.type).label
