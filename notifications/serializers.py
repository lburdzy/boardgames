from generic_relations.relations import GenericRelatedField
from rest_framework import serializers

from events.models import EntryRequest
from notifications.models import Notification


class NotificationSerializer(serializers.HyperlinkedModelSerializer):
    content_object = GenericRelatedField({
        EntryRequest: serializers.HyperlinkedRelatedField(
            queryset=EntryRequest.objects.all(),
            view_name='entryrequest-detail'
        )
    })

    class Meta:
        model = Notification
        fields = ('url', 'id', 'type', 'content_object')
