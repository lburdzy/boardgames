import pytest


@pytest.mark.django_db
def test_notification_string_representation(notification):
    assert str(notification.type_label) in str(notification)
