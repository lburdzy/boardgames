import pytest
from django.urls.base import reverse
from rest_framework import status

from events.tests.helpers import notification_in_results


@pytest.mark.django_db
def test_list_view_for_anonymous_user(
        notification_viewset, request_factory
):
    request = request_factory.get(reverse('notification-list'))
    response = notification_viewset(request)
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.django_db
def test_list_view_for_authenticated_user(
        notification_list_response
):
    assert notification_list_response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_list_view_contains_owners_notification(
        list_view_results, notification
):
    assert notification_in_results(list_view_results, notification)


@pytest.mark.django_db
def test_list_view_does_not_contain_non_owner_notification(
        list_view_results, non_participant_notification
):
    assert not notification_in_results(list_view_results, non_participant_notification)
