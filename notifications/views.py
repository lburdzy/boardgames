from django_filters.rest_framework.backends import DjangoFilterBackend
from rest_framework import viewsets

from notifications.models import Notification
from notifications.serializers import NotificationSerializer


class NotificationViewSet(viewsets.ModelViewSet):
    queryset = Notification.objects.all().order_by('modified')
    serializer_class = NotificationSerializer
    filter_backends = (DjangoFilterBackend, )
    fielter_fields = 'content_object'

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(owner=self.request.user)
