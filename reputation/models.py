from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel


class Rating(TimeStampedModel):
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_('author'), related_name='ratings',
    )
    event = models.ForeignKey(
        'events.Event', verbose_name=_('event'), related_name='ratings'
    )

    class Meta:
        verbose_name = _('rating')
        verbose_name_plural = _('ratings')
        unique_together = (
            ('author', 'event'),
        )
