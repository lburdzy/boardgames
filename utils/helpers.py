from django.contrib.gis.geos.point import Point
from django.contrib.gis.geos.polygon import Polygon


def get_point_geometry(latitude, longitude):
    return Point(longitude, latitude, srid=4326)


def get_polygon_geometry(north, west, east, south):
    args = [
        (north, west),
        (north, east),
        (south, east),
        (south, west),
        (north, west)
    ]
    point_list = [get_point_geometry(*arg) for arg in args]
    return Polygon(point_list, srid=4326)
