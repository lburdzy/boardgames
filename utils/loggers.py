import os
import logging

from django.conf import settings
from logzero import setup_logger


zs_logger = setup_logger(
    name='zs logger',
    logfile=os.path.join(settings.LOGS_DIR, 'zs_logger.log'),
    level=logging.INFO
)
