import random

from django.contrib.gis.geos.point import Point
from django.contrib.gis.geos.polygon import Polygon

from utils.helpers import get_point_geometry, get_polygon_geometry


def test_get_point_geometry_returns_correct_type():
    longitude = random.uniform(-90.0, 90.0)
    latitude = random.uniform(-180.0, 180.0)
    point = get_point_geometry(latitude, longitude)
    assert isinstance(point, Point)


def test_get_polygon_geometry_returns_correct_type():
    parameters = (10, 10, 20, 20)
    polygon = get_polygon_geometry(*parameters)
    assert isinstance(polygon, Polygon)
